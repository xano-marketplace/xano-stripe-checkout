import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {RedirectSuccessComponent} from "./redirect-success/redirect-success.component";
import {RedirectCancelComponent} from "./redirect-cancel/redirect-cancel.component";
import {SessionsComponent} from "./sessions/sessions.component";

const routes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'sessions', component: SessionsComponent},
	{path: 'success', component: RedirectSuccessComponent},
	{path: 'cancel', component: RedirectCancelComponent}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
