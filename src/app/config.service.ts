import {Inject, Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {XanoService} from "./xano.service";
import {ApiService} from "./api.service";
import {DOCUMENT} from "@angular/common";

declare let Stripe;

export interface XanoConfig {
	title: string,
	summary: string,
	editText: string,
	editLink: string,
	descriptionHtml: string,
	logoHtml: string,
	requiredApiPaths: string[]
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public stripePublicTestKey: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public stripePriceID: BehaviorSubject<any> = new BehaviorSubject<any>(null)

	public stripe;
	public config: XanoConfig = {
		title: 'Xano Stripe Checkout',
		summary: 'This demo illustrates the use of Stripe checkout process with Xano as your backend',
		editText: 'Get source code',
		editLink: 'https://gitlab.com/xano-marketplace/xano-stripe-checkout',
		descriptionHtml: `
                <h2>Description</h2>
                <p>
                	This demo illustrates the use of the Stripe Checkout process using Xano as your backend. 
                	The API mirrors all the Stripe session endpoints and includes a webhook endpoint for the Session Object.</p>
                <mark>
                	<b>
                		Note: Please make sure you are using using your Stripe <u>test</u> keys. For this demo <u>do not</u> 
                		use your live keys!
                	</b>
                </mark>
				<p class="mt-2">For test mode, use known test cards (e.g. 4242 4242 4242 4242). (Expiration can be any date in the future, CVV any 3 digit number, etc.)</p>
				<a href="https://stripe.com/docs/testing" target="_blank">Read more on Stripe Testing</a>
			    <h2 class="mt-2">Directions</h2>
				<ul>
				    <li>Install the extension in your Xano workspace.</li>
					<li>Create a <a href="https://stripe.com" target="_blank">Stripe</a> account or use an existing one.</li>
					<li>
						In the <a href="https://dashboard.stripe.com/" target="_blank">Stripe dashboard</a>,  
						in the top left-hand corner, create a new account or edit the existing account to the name of your business. 
					</li>
					<li>
						Make sure you are viewing <b>test data</b> (there is a toggle switch in the left menu of the dashboard).
					</li>
					<li>
						In your Stripe dashboard go to <a href="https://dashboard.stripe.com/apikeys">Developers —> API keys</a>. 
						Reveal and copy the secret key and set it as your 
						<code>stripe_api_secret</code>  by clicking Configure on the Stripe Checkout extension page in Xano.
					</li>
					<li>
						In the Stripe dashboard, click on <a href="https://dashboard.stripe.com/products" target="_blank"">products</a> 
						on the left hand menu and create a product with a one-time price (not recurring) and click save product. 
					</li>
					<li>
						View your recently created product and copy the <b>API ID</b> under the pricing section. 
						(it similar to <code>price_1I6kvmEUf1SsyzuGQHSt14m8</code>.  Paste it into Stripe Price ID of the demo settings window by 
						clicking test with your Xano account.
					</li>
					<li>
						Next, retrieve the <b>Stripe Public Test Key</b> which is found in <a href="https://dashboard.stripe.com/apikeys">Developers —> API keys</a>.
						Set this as the Stripe Public Test Key in the demo settings window
					</li>
					<li>
						<b>The Xano Base URL</b> is found in the newly created API group stripe_checkout in your Xano workspace.
					</li>
					<li>
						Optional: If you'd like to use the webhook functionality, go to 
						<a href="https://dashboard.stripe.com/webhooks">Developers —> Webhooks</a>. Click <b>+ Add Endpoint</b>
						Then set the url to your <code>your_xano_api_url/webhooks</code>
						and add the following event types: 
						<ul>
							<li>checkout.session.async_payment_succeeded</li>
							<li>checkout.session.async_payment_failed</li>
							<li>checkout.session.completed</li>
						</ul>
					</li>
				</ul>
				<h2 class="mt-2">Components</h2>
				<ul>
					<li>
						<b>Cart</b>
						<p>
						This component is a simple static cart that takes the price_id and quantity of how many to order. 
						You can click checkout, which will redirect you to Stripe to complete the checkout process.
						</p>
					</li>
					<li>
						<b>Redirect-Cancel</b>
						<p>A redirect route that Stripe redirects to if the checkout process is cancelled.</p>
					</li>
					<li>
						<b>Redirect-Success</b>
						<p>A redirect route that Stripe redirects to upon a successful checkout process</p>
					</li>
					<li>
					<b>Sessions</b> 
					<p>
						A list of all Sessions (Most likely, you will add authentication to both the endpoint and the 
						route, but authentication is omitted for this demo).
					</p>
					</li>
					<li>
						<b>View Panel</b>
						<p>This will show a single Session Object or a list of a sessions items.</p>
					</li>
					<li>
						<b>Config Panel</b>
						<p>This is where you’ll set your api url, your Stripe public test key, and price_id.</p>
					</li>
				</ul>
                `,
		logoHtml: '',
		requiredApiPaths: [
			'/sessions',
			'/sessions/{id}',
			'/sessions/{id}/line_items'
		]
	};
	private baseUrl: string = this.document.location.origin;
	public successUrl: string = this.baseUrl + (this.baseUrl.includes('localhost') ? '/success' : '/xano-stripe-checkout/success/');
	public cancelUrl: string = this.baseUrl + (this.baseUrl.includes('localhost') ? '/cancel' : '/xano-stripe-checkout/cancel');

	constructor(
		private apiService: ApiService,
		private xanoService: XanoService,
		@Inject(DOCUMENT) private document
	) {
		this.stripePublicTestKey.asObservable().subscribe(res => {
			if (res) {
				this.stripe = Stripe(res);
			}
		})
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
		});
	}

}
