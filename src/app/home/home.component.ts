import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from "../config.service";
import {StripeService} from "../stripe.service";
import {FormControl} from "@angular/forms";
import {get} from 'lodash-es';
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	public config: XanoConfig;
	public configured: boolean = false;
	public priceID: string;

	public quantityControl: FormControl = new FormControl(1)

	constructor(
		private configService: ConfigService,
		private stripeService: StripeService,
		private snackBar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		this.config = this.configService.config;
		this.configService.stripePriceID.asObservable().subscribe(res => {
			this.priceID = res;
		})
		this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl)
	}

	public checkout(): void {
		this.stripeService.saveSession({
			success_url: this.configService.successUrl,
			cancel_url: this.configService.cancelUrl,
			line_items: [{
				price: this.priceID,
				quantity: this.quantityControl.value
			}]
		}).subscribe(session => {
			localStorage.setItem('stripe-checkout', JSON.stringify({
				xanoApiUrl: this.configService.xanoApiUrl.value,
				stripePublicTestKey: this.configService.stripePublicTestKey.value,
				stripePriceID: this.configService.stripePriceID.value

			}));
			this.configService.stripe.redirectToCheckout({sessionId: session.id});
		},error => {
			this.snackBar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {panelClass: 'error-snack'});
		});
	}
}
