import {Component, OnInit} from '@angular/core';
import {ConfigService} from "../config.service";
import {Router} from "@angular/router";

@Component({
	selector: 'app-redirect-cancel',
	templateUrl: './redirect-cancel.component.html',
	styleUrls: ['./redirect-cancel.component.scss']
})
export class RedirectCancelComponent implements OnInit {

	constructor(
		private configService: ConfigService,
		private router: Router
	) {
	}

	ngOnInit(): void {
		const localVariables = JSON.parse(localStorage.getItem('stripe-checkout'));

		if (localVariables) {

			this.configService.stripePriceID.next(localVariables.stripePriceID);
			this.configService.stripePublicTestKey.next(localVariables.stripePublicTestKey);
			this.configService.xanoApiUrl.next(localVariables.xanoApiUrl);

			localStorage.removeItem('stripe-checkout');
		} else {
			this.router.navigate([''])
		}
	}

}
