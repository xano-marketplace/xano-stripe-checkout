import {Component, OnInit} from '@angular/core';
import {StripeService} from "../stripe.service";
import {MatDialog} from "@angular/material/dialog";
import {ViewPanelComponent} from "../view-panel/view-panel.component";
import {Router} from "@angular/router";
import {ConfigService} from "../config.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {get} from 'lodash-es';

@Component({
	selector: 'app-sessions',
	templateUrl: './sessions.component.html',
	styleUrls: ['./sessions.component.scss']
})
export class SessionsComponent implements OnInit {

	public sessions: any[] = []

	constructor(
		private configService: ConfigService,
		private stripeService: StripeService,
		private dialog: MatDialog,
		private router: Router,
		private snackBar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		this.configService.isConfigured().subscribe(res=> {
			if(!res) this.router.navigate([''])
		});

		this.stripeService.getSessions({paymentIntent: false, limit: 25, startAfter: null}).subscribe(res => {
			this.sessions = res?.data;
		}, error => this.snackBar.open(
			get(error, 'error.message', 'An Error Occurred'), 'Error', {panelClass: 'error-snack'}));
	}

	public openPanel(type, sessionID): void {
		this.dialog.open(ViewPanelComponent, {data: {type, sessionID}, minWidth: '50vw'})
	}

}
