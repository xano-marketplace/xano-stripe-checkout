import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {ConfigService} from './config.service';
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class StripeService {

	constructor(private apiService: ApiService, private configService: ConfigService) {
	}

	public getSessions(sessions: { paymentIntent: boolean, startAfter: string, limit: number }): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/sessions`,
			params: sessions
		});
	}

	public getSession(id: string): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/sessions/${id}`,
		});
	}

	public getSessionLineItems(lineItems: { id: string, limit: number, start_after: string }): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/sessions/${lineItems.id}/line_items`,
			params: lineItems
		});
	}

	public saveSession(session: { success_url: string, cancel_url: string, line_items: any[] }): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/sessions`,
			params: session,
		});
	}
}
