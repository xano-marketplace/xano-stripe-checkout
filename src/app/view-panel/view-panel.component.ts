import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {StripeService} from "../stripe.service";
import {get} from 'lodash-es';
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
	selector: 'app-view-panel',
	templateUrl: './view-panel.component.html',
	styleUrls: ['./view-panel.component.scss']
})
export class ViewPanelComponent implements OnInit {

	public result: any;

	constructor(
		@Inject(MAT_DIALOG_DATA) public data,
		private stripeService: StripeService,
		private snackBar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		if(this.data?.type && this.data?.sessionID) {
			if (this.data.type === 'items') {
				this.stripeService.getSessionLineItems({id: this.data.sessionID, limit: 25, start_after: null})
					.subscribe(res => {
						this.result = res.data
					}, error => this.snackBar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {panelClass: 'error-snack'}));
			} else {
				this.stripeService.getSession(this.data.sessionID).subscribe(res => {
					this.result = res
				})
			}
		}
	}

}
